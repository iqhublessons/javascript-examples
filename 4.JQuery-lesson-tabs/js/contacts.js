//var contactList = [
//	{
//		logo: "img/girl-1.jpg",
//		name: "Дарт Вэйдер (новая полиция)",
//		tel: "+38 053 6666102"
//	},
//	{
//		logo: "img/girl-2.jpg",
//		name: "Принцесса Лея (тушить пожар)",
//		tel: "+38 053 6666101"
//	},
//	{
//		logo: "img/girl-3.jpg",
//		name: "Люй Скайвокер (медбрат)",
//		tel: "+38 053 6666103"
//	},
//	{
//		logo: "img/girl-4.jpg",
//		name: "Темный император (газовая служба)",
//		tel: "+38 053 6666104"
//	}
//];


// readyState
//0 - Unitialized
//1 - Loading
//2 - Loaded
//3 - Interactive
//4 - Complete

//=========================================================================

"use strict";

var divContactList;

function init() {
	divContactList = document.querySelector(".contact-list");
	getContacts(function(err, data){
		// console.log(data);
		if (err) return console.error("Please run it from real server!");
		var receivedContacts;
		try {
			receivedContacts = JSON.parse(data);
		} catch(e) {
			return console.error(e.message);
		}
		renderContacts(receivedContacts);
	});
}

function getContacts(cb) {
	var req = new XMLHttpRequest();
	req.open("GET", "getContacts", true);
	// or onload
	req.onreadystatechange = function() {
		if (this.readyState === 4) {
			if (this.status === 200) {
				cb(null, this.responseText);
			}
		}
	};
	req.onerror = function(e) {
		console.error(e);
		cb(e);
	};
	req.send();
}

init();



// function init() {
// 	$.ajax({
// 		url: "/getContacts",
// 		type: "GET",
// 		success: function(msg) {
// 			try {
// 				msg = JSON.parse(msg);
// 				console.log(msg);
// 			} catch(e) {
// 				return console.error("JSON parsing exception:", e.message);
// 			}
// 			renderContacts(msg);
// 		},
// 		error: function(e) {
// 			console.error(e);
// 		}
// 	});
// }



function renderContacts(contactList) {
	var divContactList = $(".contact-list");
	divContactList.html("");

	var format =  '<div class="media">' +
		'<div class="media-left">' +
		'<img class="media-object" src="{{LOGO_URL}}">' +
		'</div>' +
		'<div class="media-body">' +
		'<h4 class="media-heading">{{NAME}}</h4>' +
		'<span>{{TEL}}</span>' +
		'</div></div>';


	for (var i = 0; i < contactList.length; i++) {
		divContactList.append(format
			.replace("{{LOGO_URL}}", contactList[i].logo)
			.replace("{{NAME}}", contactList[i].name)
			.replace("{{TEL}}", contactList[i].tel)
		);
	}
}

// function renderContacts(contactList) {
// 	for (var i = 0; i < contactList.length; i++) {
// 		divContactList.innerHTML += '<div class="media">' +
// 			'<div class="media-left">' +
// 			'<img class="media-object" src="' + contactList[i].logo +'">' +
// 			'</div>' +
// 			'<div class="media-body">' +
// 			'<h4 class="media-heading">' + contactList[i].name + '</h4>' +
// 			'<span>' + contactList[i].tel + '</span>' +
// 			'</div></div>';
// 	}
// }