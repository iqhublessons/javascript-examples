// var ulNavTabs = document.querySelectorAll("ul.nav.nav-tabs a");
// var divNavContent = document.querySelector("div.nav-content");
//
// function init() {
// 	for (var i = 0; i < ulNavTabs.length; i++) {
// 		ulNavTabs[i].addEventListener("click", tabClick);
// 		if (ulNavTabs[i].parentNode.classList.contains("active")) {
// 			showTab(ulNavTabs[i].getAttribute("content-tab"));
// 		}
// 	}
// }
//
// function showTab(name) {
// 	for (var i = 0; i < divNavContent.children.length; i++) {
// 		var navContentItem = divNavContent.children[i];
// 		if (navContentItem.getAttribute("content-tab-name") === name) {
// 			navContentItem.style.display = "block";
// 		} else {
// 			navContentItem.style.display = "none";
// 		}
// 	}
// }
//
// function tabClick(event) {
// 	var navContentItemName = event.target.getAttribute("content-tab");
// 	showTab(navContentItemName);
// 	setTabDisplayActive(navContentItemName);
// }
//
// function setTabDisplayActive(tabItem) {
// 	for (var i = 0; i < ulNavTabs.length; i++) {
// 		if (ulNavTabs[i].getAttribute("content-tab") === tabItem) {
// 			ulNavTabs[i].parentNode.classList.add("active");
// 		} else {
// 			if (ulNavTabs[i].parentNode.classList.contains("active")) {
// 				ulNavTabs[i].parentNode.classList.remove("active");
// 			}
// 		}
// 	}
// }
//
// init();


// function init() {
// 	$(".nav-content-item").hide();
// 	$(".nav-content-item[content-tab-name='" + $(".nav-tabs li.active").find("a").attr("content-tab") + "']").show();
// 	$(".nav-tabs li a").click(function(e){
// 		$(".nav-content-item[content-tab-name='" + $(".nav-tabs li.active a").attr("content-tab") + "']").fadeOut(100);
// 		$(".nav-tabs li").removeClass("active");
// 		$(this).parent().addClass("active");
// 		// $(".nav-content-item").fadeOut();
// 		$(".nav-content-item[content-tab-name='" + $(this).attr("content-tab") + "']").fadeIn(400);
// 	});
// }
//
// init();


function initAnim() {
	$(".nav-content-item").hide();
	$(".nav-content-item[content-tab-name='" + $(".nav-tabs li.active").find("a").attr("content-tab") + "']").show();
	$(".nav-tabs li a").click(function(e){
		var self = this;
		$(".nav-content-item[content-tab-name='" + $(".nav-tabs li.active a").attr("content-tab") + "']").animate({
			opacity: 0,
			left: "100px"
		}, 500 ,function(){
			$(this).hide();
			$(".nav-content-item[content-tab-name='" + $(self).attr("content-tab") + "']")
				.css({"display": "block", "opacity": "0", "left": "100px"})
				.animate({
						opacity: 1,
						left: 0
					},
					500
				);
		});
		$(".nav-tabs li").removeClass("active");
		$(this).parent().addClass("active");
	});
}
initAnim();


// function initAnimTransform() {
// 	$(".nav-content-item").hide();
// 	$(".nav-content-item[content-tab-name='" + $(".nav-tabs li.active").find("a").attr("content-tab") + "']").show();
// 	$(".nav-tabs li a").click(function(e){
// 		var self = this;
// 		$(".nav-content-item[content-tab-name='" + $(".nav-tabs li.active a").attr("content-tab") + "']").animate(
// 			{
// 				opacity: 0,
// 				left: "100px"
// 			},
// 			{
// 				step: function(now, fx) {
// 					$(this).css("transform", "scale(" + ( 1 - (now/100)) + ")");
// 					// console.log(now/100);
// 				},
// 				complete: function() {
// 					$(this).hide();
// 					$(".nav-content-item[content-tab-name='" + $(self).attr("content-tab") + "']")
// 						.css({"display": "block", "opacity": "0", "left": "100px", "transform": "scale(1)"})
// 						.animate(
// 							{
// 								opacity: 1,
// 								left: 0
// 							},
// 							500,
// 							"swing"
// 						);
// 				}
// 			},
// 			500,
// 			"swing"
// 		);
// 		$(".nav-tabs li").removeClass("active");
// 		$(this).parent().addClass("active");
// 	});
// }

// initAnimTransform();
