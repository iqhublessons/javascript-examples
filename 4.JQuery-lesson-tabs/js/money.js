"use strict";

$("#form-money").submit(function(event){
	event.preventDefault();
	var inputExchangeRateValue = $(this).find("#exchange-rate").val();
	var inputSumValue = $(this).find("#sum").val();
	var logString = "";
	var outSum;
	inputExchangeRateValue = parseFloat(inputExchangeRateValue);
	inputSumValue = parseFloat(inputSumValue);
	outSum = (inputExchangeRateValue * inputSumValue).toFixed(2);
	logString += inputExchangeRateValue + " * " + inputSumValue + " = " + outSum;
	$(".list-operations").append($('<li class="list-group-item"></li>').html(logString).hide().fadeIn().click(function(){
		$(this).fadeOut(function(){
			$(this).remove();
		});
	}));
});



// document.querySelector("#form-money").onsubmit = calculateExchange;
// document.querySelector("#form-money").addEventListener("submit", calculateExchange);
// function calculateExchange(e) {
// 	e.preventDefault();

// 	var formMoney = document.querySelector("#form-money");
// 	var listOperations = document.querySelector(".list-operations");
// 	var inputExchangeRateValue = formMoney.querySelector("#exchange-rate").value;
// 	var inputSumValue = formMoney.querySelector("#sum").value;
// 	var logString = "";
// 	var outSum;

// 	inputExchangeRateValue = parseFloat(inputExchangeRateValue);
// 	inputSumValue = parseFloat(inputSumValue);
// 	outSum = (inputExchangeRateValue * inputSumValue).toFixed(2);
// 	logString += inputExchangeRateValue + " * " + inputSumValue + " = " + outSum;

// 	var logItem = document.createElement("li");
// 	logItem.classList.add("list-group-item");
// 	logItem.innerHTML = logString;
// 	logItem.addEventListener("click", function(e){
// 		this.remove();
// 	});
// 	listOperations.appendChild(logItem);
// }