// camelCase
// under_score

// BAD variable names!!!
// var 1user = "Sasha";
// var first-user = "Alex";

"use strict";

/*	INTRO
=============================*/

// console.log("da");
// console.log("net");


// console.log(
// 	3
// 	+ 4
// 	+ 1
// );


// before '[' js doesn't put ','
// alert("Begin array");
// [1,2,3,4].forEach(alert);


/*	user's dialog
=============================*/

alert("This is alert");
console.log( prompt("How old are you?") );
console.log(prompt("How old are you?", 222));
console.log(confirm("Are you ready?"));

/*	vars
=============================*/

var COLOR_GREEN = "#0F0";
var COLOR_BLUE = "#00F";


var someVar;
someVar = "Hi guys!";

console.log(someVar);

var userName = "John",
	userSurname = "Smith",
	userAge = 14;

console.log(userName + " " + userSurname + " " + userAge);

someVar = "Hi all!";

console.log(someVar);

var _ = "Good ",
	$ = "morning ",
	ы = "guys";

console.log(_ + $ + ы);


/*	typeof
=============================*/

typeof undefined // "undefined"
typeof 0 // "number"
typeof true // "boolean"
typeof "foo" // "string"
typeof {} // "object"
typeof [] // "object"
typeof null // "object"
typeof function(){} // "function"

var undefVar;
console.log("undefVar:", undefVar);

var address = null;
console.log("address:", address);

var num = 100500;
num += 0.00001;
console.log("num:", num);

console.log("==== errors ====");
console.log(1/0);
console.log("string" * 10);
console.log("==== /errors =====");

var string = "I'm a string!";
string += ' And you?';

console.log("string:", string);

var isChecked = true;
isChecked = false;
console.log("isChecked:", isChecked);

var randomArray = [true, false, "string", null, undefined, 123, 456, NaN, Infinity];
console.log("randomArray:", randomArray);

var userProfile = {
	name: "Kindrat",
	surname: "Krutoy",
	age: 32,
	gender: "male",
	isMarried: true,
	work: undefined,
	hobby: ["пиво", "футбол", "пельмени"]
}
console.log("userProfile:", userProfile);


/*	manipulations
=============================*/

var digit = 1;

digit = digit + 3;
console.log(digit);
digit = digit - 2;
console.log(digit);
digit = digit * 3;
console.log(digit);
digit = digit / 2;
console.log(digit);

var digit = 5;

digit += 10;
console.log(digit);
digit -= 5;
console.log(digit);
digit *= 2;
console.log(digit);
digit /= 2;
console.log(digit);


digit = -digit;
console.log("digit:", digit);
console.log(++digit);
console.log(digit++);
console.log(digit);
console.log(--digit);

var stringDigit = "44";
console.log(stringDigit + 5);
console.log(parseInt(stringDigit) + 5);
var string = "sold: " + 4*3 + 2 + " items"; 
console.log(string);
console.log("sold: " + (4*3+2) + " items");

console.log(+true);
console.log(+false);
console.log(+"100500");
console.log(Number("100500"));

console.log(!true);
console.log(!false);

console.log(!!"0");
console.log(!!" ");

/*	logic operators
=============================*/

// >  >=  <  <=  ==  ===

console.log(10 <= 10);

console.log("h" > "H");
console.log("H" > "H");

console.log("Albert" > "Alex");
console.log("Albert" < "Kindrat");

console.log("4" > "345");

console.log("5" == 5);
console.log("5" === 5);
console.log(5 === 5);

console.log("5" != 5);
console.log("5" !== 5);

var digit = 45;
var name = "This is sparta!!!!!";

if (name.length > 10) {
	console.log("too long string!");
} else {
	console.log("Good name!");
}

name.length > 10 ? console.log("too long string") : console.log("Good name!");


if (digit <= 40) {
	console.log("It's less!");
} else if (digit >= 50) {
	console.log("It's more!");
} else {
	console.log("It's between!");
}

if (digit <= 40 || digit >= 50) {
	console.log("True range!");
} else {
	console.log("False range!");
}

(digit <= 40 || digit >= 50) ? console.log("True range") : console.log("False range");

if (name && digit) {
	console.log(name + " " + digit);
} else {
	console.log("Failed");
}

(name && digit) ? console.log(name + " " + digit) : console.log("Failed");

var size = 111604133888;

if (size <= 1024) {
	console.log(size.toFixed(2) + " B");
} else {
	size /= 1024;
	if (size <= 1024) {
		console.log(size.toFixed(2) + " KB");
	} else {
		size /= 1024;
		if (size <= 1024) {
			console.log(size.toFixed(2) + " MB");
		} else {
			size /= 1024;
			if (size <= 1024) {
				console.log(size.toFixed(2) + " GB");
			} else {
				size /= 1024;
				console.log(size.toFixed(2) + " TB");
			}
		}
	}
}

/*	cycles
=============================*/

var count = 4;

for (var i = 0; i < count; i++) {
	console.log("for: " + i);
}


while (count >= 0) {
	console.log("while: " + count);
	count--;
}

count = 0;

do {
	console.log("do-while:" + count);
	count++;
} while (count < 4);

var hobbies = ["водка", "телки", "попиросы", "вот и все мои запросы", ":)"];
console.log("hobbies size:", hobbies.length);

for (var i = 0; i < hobbies.length; i++) {
	console.log((i+1) + ". " + hobbies[i]);
}

for (var i in hobbies) {
	console.log((+i+1) + ". " + hobbies[i]);
}

var userProfile = {
	name: "Kindrat",
	surname: "Krutoy",
	age: 32,
	gender: "male",
	isMarried: true,
	work: undefined,
	hobby: ["пиво", "футбол", "пельмени"]
}

for (var i in userProfile) {
	console.log(i + ": " + userProfile[i]);
}

hobbies["name"] = "Vanya";
hobbies["some"] = "value";

for (var i in hobbies) {
	console.log(i + ". " + hobbies[i]);
}


var thisArray = [1, 2, 3, 4, "name", 5, 10, 50, 100];

for (var i = 0; i < thisArray.length; i++) {
	if (thisArray[i] === "name") {
		continue;
	}
	console.log(thisArray[i]);
}

for (var i = 0; i < thisArray.length; i++) {
	var item = thisArray[i];
	if (typeof(item) === "string") {
		break;
	}
	console.log(item);
}

/*	switch
=============================*/

var hobbies = ["водка", "попиросы", "вот и все мои запросы", ":)"];

switch(hobbies[0]) {
	case "водка": 
		console.log("Хана печени!");
		break;
	case "попиросы":
		console.log("Хана легким!");
		break;
	default:
		console.log("Хмм.. Вы что, не пьете и не курите?!");
}

/*	function
=============================*/

function getSome() {
	return "This is string";
}

var getSome = function() {
	return "This is string";
}

console.log(getSome());

function concat(str1, str2, str3) {
	return str1 + str2 + str3;
}

var concat2 = function(str1, str2, str3) {
	return str1 + str2 + str3;
}

console.log(concat("I ", "am ", "hero!"));

function concat2(str1, str2, str3) {
	if (!str1 || !str2 || !str3) {
		console.error("BAD PARAMETERS!");
		return "";
	}
	return str1 + str2 + str3;
}

console.log(concat2("sdsadas"));


function args() {
	console.log(arguments);
	console.log("total arguments: ", arguments.length);
}

args(1,2,3,4,5, "more", "davay na more");



var userProfile = {
	name: "Kindrat",
	surname: "Krutoy",
	age: 32,
	gender: "male",
	isMarried: true,
	work: undefined,
	hobby: ["пиво", "футбол", "пельмени"],

	getGender: function() {
		switch(this.gender) {
			case "male":
				return "Мужик";
				break;
			case "female":
				return "Женщина";
				break;
			default:
				return "Не определился";
		}
	}

}

console.log(userProfile.getGender());