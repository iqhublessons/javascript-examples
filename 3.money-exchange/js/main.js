var formMoney = document.querySelector("#form-money"); // получаем ссылку на тег формы
var listOperations = document.querySelector(".list-operations"); // получаем ссылку на div с информацией об истории операций
var divAlert = document.querySelector(".exchange-error"); // получаем ссылку на div с текстом ошибки
var inputExchangeRateValue = formMoney.querySelector("#exchange-rate"); // получаем ссылку на input текущего курса за у.е.
var inputSumValue = formMoney.querySelector("#sum"); // получаем ссылку на input суммы у.е.
var buttonSubmitForm = formMoney.querySelector("button"); // получаем ссылку на кнопку отправки результатов формы

// функция-обработчик формы
function calculateExchange(e) {
	// отменяем действие action-а (для предотвращения перехода по аттрибуту action, который все равно не был указан)
	e.preventDefault();

	// получаем значение поля #exchange-rate
	var exchangeRateValue = inputExchangeRateValue.value;
	// получаем значение поля #sum
	var sumValue = inputSumValue.value;
	// объявляем пустую строку, в которой будем формировать текст для вывода
	var logString = "";
	// переменная, в которой будем хранить результат операции умножения курса на сумма
	var outSum;

	// проверяем ЕСЛИ пользователь не ввел ничего в одно из двух полей
	// аналогично можно было бы написать if (exchangeRateValue.length === 0 || sumValue === 0)
	if (!exchangeRateValue || !sumValue) {
		// показываем ошибку, вызывая функцию, которая ее покажет
		showAlert("Все поля должны быть заполнены!");
		// завершаем выполнение текущей функции calculateExchange путем возврата пустого значения
		return;
	}

	// функция isNaN(переменная) пытается преобразовать переменную в число, если не получается этого сделать - возвращает true (NaN - невозможно вычислить, соответственно true когда вычисление невозможно)
	if (isNaN(exchangeRateValue) || isNaN(sumValue)) {
		// показываем ошибку, вызывая функцию, которая ее покажет
		showAlert("Некорректное значение!");
		// завершаем выполнение текущей функции calculateExchange путем возврата пустого значения
		return;
	}

	// преобразовываем переменные из строкового типа в числовой (из "string" в "number")
	exchangeRateValue = parseFloat(exchangeRateValue);
	sumValue = parseFloat(sumValue);
	
	// выполняем умножение
	outSum = exchangeRateValue * sumValue;
	// приводим к читабельному виду, ограничивая количество цифр после запятой двумя
	outSum = outSum.toFixed(2);

	// формируем строку с информацией об операции
	logString += exchangeRateValue + " * " + sumValue + " = " + outSum;

	// выводим в консоль строку
	console.log("logString:", logString);

	// добавляем строку в виде HTML-тега li в ul список
	listOperations.innerHTML += '<li class="list-group-item">' + logString + '</li>';
}

// функция показа ошибки
function showAlert(message) {
	// выводим внутрь div-а с ошибкой необходимый текст
	divAlert.innerHTML = message;
	// показываем блок с ошибкой (так как изначально в css мы задали этому блоку display: none)
	divAlert.style.display = "block";
	// отключаем возможность ввода значений в input-ы на время показа ошибки
	inputExchangeRateValue.disabled = true;
	inputSumValue.disabled = true;
	// отключаем нажатие на кнопку отправки формы на время показа ошибки
	buttonSubmitForm.disabled = true;
	// устанавливаем таймер на 2 секунды, по истечению времени которого выполнится вложенная анонимная функция

	// функция, задача которой включить снова все input-ы и кнопку, очистить блок от показанной ошибки и скрыть его
	function whenTimeOut() {
		// очищаем div от текста с описанием ошибки
		divAlert.innerHTML = "";
		// скрываем его
		divAlert.style.display = "none";
		// включаем обратно все элементы формы
		inputExchangeRateValue.disabled = false;
		inputSumValue.disabled = false;
		buttonSubmitForm.disabled = false;
	}

	// устанавливаем таймер, по истечению времени которого выполнится функция whenTimeOut
	setTimeout(whenTimeOut, 2000);	
}